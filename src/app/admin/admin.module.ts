import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { GestionFormationComponent } from './gestion-formation/gestion-formation.component';
import { GestionCompteComponent } from './gestion-compte/gestion-compte.component';
import { ListeCandidatsComponent } from './liste-candidats/liste-candidats.component';
import { AdminComponent } from './admin/admin.component';


@NgModule({
  declarations: [GestionFormationComponent, GestionCompteComponent, ListeCandidatsComponent, AdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
