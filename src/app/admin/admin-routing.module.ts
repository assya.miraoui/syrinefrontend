import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GestionFormationComponent} from './gestion-formation/gestion-formation.component';
import {GestionCompteComponent} from './gestion-compte/gestion-compte.component';
import {ListeCandidatsComponent} from './liste-candidats/liste-candidats.component';
import {AdminComponent} from './admin/admin.component';


const routes: Routes = [
  {path : '', component : AdminComponent, children :
    [
  {
    path : 'GestionFormation',
    component : GestionFormationComponent,
  },

  {
    path : 'GestionCompteFormateurs',
    component : GestionCompteComponent,
  },

  {
    path : 'listeCandidat',
    component : ListeCandidatsComponent,
  }

  ]}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
