import { Component, OnInit } from '@angular/core';
import {CandidatService} from '../../services/candidat.service';

@Component({
  selector: 'app-liste-candidats',
  templateUrl: './liste-candidats.component.html',
  styleUrls: ['./liste-candidats.component.css']
})
export class ListeCandidatsComponent implements OnInit {
cand ;
  constructor(private candidatServ : CandidatService) { }

  ngOnInit() {
    this.candidatServ.getAll().subscribe(res=>{
      console.log(res);
      this.cand = res;

    })


  }

}
