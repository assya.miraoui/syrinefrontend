import { Component, OnInit } from '@angular/core';
import {FormationService} from '../../services/formation.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CandidatService} from '../../services/candidat.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  loginForm: FormGroup;
  error = '';
  submit = false;
  formation;
  user;
  constructor(private loginServ: CandidatService, private formBuilder: FormBuilder,private formationservice : FormationService
  ,private router : Router) {


    this.user = localStorage.getItem('user');
    console.log(this.user);


  }

  ngOnInit() {

    this.formationservice.getAll().subscribe(res=>{

      console.log(res);
      this.formation = res;
    });
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      motdepasse: ['', Validators.required]
    })
  }
  get f() {

    return this.loginForm.controls;
  }


  auth() {
    this.submit = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {

      return;
    }


    this.loginServ.login(this.loginForm.value['email'], this.loginForm.value['motdepasse']).subscribe(res => {
      window.location.reload();

      if (JSON.parse(JSON.stringify(res)).status === 'success'){

        localStorage.setItem('user', JSON.stringify(JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).data)).user));
        localStorage.setItem('token', JSON.stringify(JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).data)).token));

      }
      console.log(res);


    })

  }


  Logout(){


    this.loginServ.logout();
window.location.reload()
    this.router.navigate(['/'])

  }


}
