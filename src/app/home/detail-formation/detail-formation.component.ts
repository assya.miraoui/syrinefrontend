import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormationService} from '../../services/formation.service';

@Component({
  selector: 'app-detail-formation',
  templateUrl: './detail-formation.component.html',
  styleUrls: ['./detail-formation.component.css']
})
export class DetailFormationComponent implements OnInit {
id;
detail;
aa;
  constructor(private activeRoute : ActivatedRoute, private formationservice : FormationService) {
   this.id =  this.activeRoute.params['value']['id']
    console.log(this.id);
  }

  ngOnInit() {
    this.formationservice.getById(this.id).subscribe(res=>{
      console.log(res);
      this.detail = res;
      this.aa = this.detail.niveau;
      console.log(this.aa);

    });
    this.getColor(this.aa);
  }


  getColor(aa) {
    switch (this.aa) {
      case 'Debutant':
        return 'green';
      case 'Intermediare':
        return 'orange';
      case 'Avancée':
        return 'red';
    }
  }

}
