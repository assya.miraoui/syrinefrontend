import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router} from '@angular/router';
import {CandidatService} from '../services/candidat.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  error = '';
  submit = false;

  constructor(private loginServ: CandidatService, private formBuilder: FormBuilder,
              private route: Router) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  get f() {

    return this.loginForm.controls;
  }


  auth() {
    this.submit = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {

      return;
    }


    this.loginServ.login(this.loginForm.value['email'], this.loginForm.value['password']).subscribe(res => {

      if (JSON.parse(JSON.stringify(res)).status === 'success'){

        localStorage.setItem('user', JSON.stringify(JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).data)).user));
        localStorage.setItem('token', JSON.stringify(JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(res)).data)).token));

      }
      console.log(res);


    })

  }
}









