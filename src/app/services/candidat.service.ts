import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/internal/operators';
import {Observable} from 'rxjs';
import {JavaScriptEmitter} from '@angular/compiler/src/output/js_emitter';

@Injectable({
  providedIn: 'root'
})
export class CandidatService {
baseUrl = environment.baseUrl;
  constructor(private http : HttpClient) { }


  login(email , password) {


    console.log(this.baseUrl + 'admin/login',{email : email , password : password})

    return this.http.post(this.baseUrl + 'admin/login',{email : email , password : password})

  }

  logout() {
    localStorage.removeItem('user');
  }





  register(data, fileToUpload){
    let formData = new FormData();
    formData.append('nom', ''+data.nom);
    formData.append('prenom', ''+data.prenom);
    formData.append('email', ''+data.email);
    formData.append('password', ''+data.password);
    formData.append('CIN', ''+data.CIN);
    formData.append('CV', ''+data.CV);
    formData.append('photo', fileToUpload, fileToUpload.name);


    return this.http.post(this.baseUrl + 'candidat/ajouterCandidat', formData, fileToUpload)
  }


  getAll(){

    return this.http.get(this.baseUrl + 'candidat/getAll')

  }



}
