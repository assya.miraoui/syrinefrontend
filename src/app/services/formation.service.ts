import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FormationService {
baseUrl = environment.baseUrl;
  constructor(private http : HttpClient) { }

/*
  afficher la liste des formations
*/
  getAll(){

   return this.http.get(this.baseUrl + 'formation/getAll');
  }
  getById(id){
   return this.http.get(this.baseUrl + 'formation/getById/'+id)
  }



}
