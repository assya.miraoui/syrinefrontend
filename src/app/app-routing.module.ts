import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {MenuComponent} from './home/menu/menu.component';
import {DetailFormationComponent} from './home/detail-formation/detail-formation.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';




const routes: Routes = [


  {path :'admin', loadChildren : './admin/admin.module#AdminModule'},


  {path : '', component  :MenuComponent},
  {path : 'Home', component  : HomeComponent},
  {path : 'detailFormation/:id', component  : DetailFormationComponent},
  {path : 'login', component  :LoginComponent},
  {path : 'register', component  :RegisterComponent},

];





@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
