import { Component, OnInit } from '@angular/core';
import {CandidatService} from '../services/candidat.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  error = '';
  submit = false;
  formation;
  fileToUpload = null;

  constructor(private candidatServ: CandidatService, private formBuilder: FormBuilder
  ,private router : Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      motdepasse: ['', Validators.required],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      CIN: ['', Validators.required],
      photo: ['', Validators.required],
      CV: ['', Validators.required]
    });
  }

  get f (){
    return this.registerForm.controls;
  }

  inscrire() {
this.submit = true;

if (this.registerForm.invalid) {
  console.log(this.registerForm.value)

  return ;
  }
this.candidatServ.register(this.registerForm.value, this.fileToUpload).subscribe(res => {

      console.log(res);
      this.router.navigate(['/login']);

    });

  }
  uploadFile(files :  FileList){
    this.fileToUpload =  files.item(0);
    console.log(this.fileToUpload)

  }



}
